DIR="$(readlink -f $(dirname $0)/..)"

SCRIPTNAME=${0##*/}

error() {
  echo "ERROR: $*!"
  exit 1
}

source "$DIR"/config/multiseat.conf
