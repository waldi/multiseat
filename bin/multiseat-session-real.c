#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <pty.h>
#include <sched.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <linux/capability.h>

char *environment[] = {
  "PATH=/usr/sbin:/sbin:/usr/bin:/bin",
  NULL,
  NULL,
  NULL,
  NULL
};

void interrupt_handler(int sig __attribute__((unused)))
{
}

static void vlog(FILE *out, char *fmt, char *message, va_list ap)
{
  char *s;
  vasprintf(&s, message, ap);
  fprintf(out, fmt, s);
  fflush(out);
  free(s);
}

__attribute__((noreturn))
static void critical(char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vlog(stderr, "CRITICAL: %s\n", fmt, ap);
  va_end(ap);
  exit(1);
}

__attribute__((unused))
static void error(char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vlog(stderr, "ERROR: %s\n", fmt, ap);
  va_end(ap);
}

static void info(char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vlog(stdout, "INFO: %s\n", fmt, ap);
  va_end(ap);
}

static void debug(char *fmt, ...)
{
  return;
  va_list ap;
  va_start(ap, fmt);
  vlog(stdout, "DEBUG: %s\n", fmt, ap);
  va_end(ap);
}

static int vsystem(char *fmt, ...)
{
  va_list ap;
  char *s;
  int ret;

  va_start(ap, fmt);
  vasprintf(&s, fmt, ap);
  va_end(ap);
  debug("Executing %s", s);
  ret = system(s);
  debug("Result %d", ret);
  free(s);
  return ret;
}

static int pivot_root(const char *new_root, const char *put_old)
{
  return syscall(SYS_pivot_root, new_root, put_old);
}
static pid_t _clone(int flags)
{
  return syscall(SYS_clone, flags, NULL);
}

static void prepare_mounts(char *root)
{
  debug("Preparing mounts");
  mount(root, "/mnt", "none", MS_BIND, 0);
  pivot_root("/mnt", "/mnt/mnt");
  chdir("/");
  mount("none", "/dev/pts", "devpts", 0, "newinstance,ptmxmode=666");
}

static int run(int controlfd, char *cmd)
{
  bool break_child = false;
  int status, amaster, pollnr = 3;
  pid_t child;
  struct pollfd p[] = {
    { controlfd, 0, 0 },
    { 0, POLLIN, 0 },
    { 0, POLLIN, 0 },
  };

  void child_sigaction(int sig __attribute__((unused)), siginfo_t *siginfo, void *d __attribute__((unused)))
  {
    waitpid(siginfo->si_pid, &status, WNOHANG);
    if (siginfo->si_pid == child)
      break_child = true;
  }

  struct sigaction act;
  sigfillset(&act.sa_mask);
  act.sa_flags = SA_SIGINFO;
  act.sa_sigaction = child_sigaction;
  sigaction(SIGCHLD, &act, NULL);

  info("Executing %s", cmd);

  child = forkpty(&amaster, NULL, NULL, NULL);
  p[1].fd = amaster;
 
  if (child < 0)
    critical("fork failed %d", errno);

  if (child == 0)
  {
    execle(cmd, cmd, NULL, environment);
    _exit(1);
  }

  while (!break_child)
  {
    char buf[1024];
    ssize_t len;

    if (poll(p, pollnr, -1) < 0)
    {
      if (errno == EINTR)
        continue;
      critical("poll failed %d", errno);
    }

    if (p[1].revents & POLLIN)
    {
      len = read(p[1].fd, buf, sizeof(buf));
      if (len)
      {
        fwrite(buf, len, 1, stdout);
        fflush(stdout);
      }
      else
        return 0;
    }
    if (p[2].revents & POLLIN)
    {
      len = read(p[2].fd, buf, sizeof(buf));
      if (len)
        write(amaster, buf, len);
      else
        pollnr--;
    }
    if (p[0].revents & POLLHUP)
      critical("Got close event");
  }

  debug("Result %d", status);
  if (WIFEXITED(status))
    return WEXITSTATUS(status);
  return 1;
}

#define SECBIT_NOROOT                   1 << 0
#define SECBIT_NOROOT_LOCKED            1 << 1
#define SECBIT_NO_SETUID_FIXUP          1 << 2
#define SECBIT_NO_SETUID_FIXUP_LOCKED   1 << 3
#define SECBIT_KEEP_CAPS                1 << 4
#define SECBIT_KEEP_CAPS_LOCKED         1 << 5

static void caps_drop(int caps[])
{
  for (; *caps >= 0; caps++)
    if (prctl(PR_CAPBSET_DROP, *caps))
      critical("Failed to drop capability %d from bounding set: %s", *caps, strerror(errno));
}

void init(int controlfd, char *prepare_cmd, char *run_cmd)
{
  int drop_caps[] = {
    CAP_SETPCAP,
    CAP_LINUX_IMMUTABLE,
    CAP_SYS_MODULE,
    CAP_SYS_RAWIO,
    CAP_SYS_CHROOT,
    CAP_SYS_PTRACE,
    CAP_SYS_PACCT,
    CAP_SYS_BOOT,
    CAP_SYS_NICE,
    CAP_SYS_RESOURCE,
    CAP_SYS_TIME,
    CAP_SYS_TTY_CONFIG,
    CAP_MKNOD,
    CAP_LEASE,
    CAP_AUDIT_WRITE,
    CAP_AUDIT_CONTROL,
    CAP_SETFCAP,
    CAP_MAC_OVERRIDE,
    CAP_MAC_ADMIN,
    -1
  };
  int drop_caps_run[] = {
    CAP_SYS_ADMIN,
    -1
  };

  caps_drop(drop_caps);

  if (run(controlfd, prepare_cmd))
    critical("Prepare command failed");

  caps_drop(drop_caps_run);

  if (run(controlfd, run_cmd))
    critical("Run command failed");
}

int main(int argc, char *argv[])
{
  int status, controlfd[2], envid = 1;
  pid_t child;
  char *id, *root, *net_dev, *net_mac, *prepare_cmd, *run_cmd,
       net_vdev[] = "seat-XXXXXX";

  if (argc < 7)
    critical("Too less arguments");

  id = argv[1];
  root = argv[2];
  net_dev = argv[3];
  net_mac = argv[4];
  prepare_cmd = argv[5];
  run_cmd = argv[6];

  asprintf(&environment[envid++], "SEATID=%s", id);

  mktemp(net_vdev);
  if (!strlen(net_vdev))
    critical("Can't create name of temporary network device");

  struct sigaction act;
  sigfillset(&act.sa_mask);
  act.sa_flags = 0;
  act.sa_handler = interrupt_handler;
  sigaction(SIGINT, &act, NULL);
  sigaction(SIGTERM, &act, NULL);

  pipe2(controlfd, O_CLOEXEC);

  child = _clone(CLONE_NEWIPC|CLONE_NEWNET|CLONE_NEWNS|CLONE_NEWPID|SIGCHLD);

  if (child < 0)
    critical("Clone failed");

  if (child == 0)
  {
    char buf[1];
    close(controlfd[1]);
    read(controlfd[0], buf, 1);
    if (vsystem("ip link set %s name eth0", net_vdev))
      critical("Failed to rename network device");
    prepare_mounts(root);
    init(controlfd[0], prepare_cmd, run_cmd);
    _exit(1);
  }

  close(controlfd[0]);
  if (vsystem("ip link add link %s name %s address %s type macvlan", net_dev, net_vdev, net_mac))
    critical("Failed to create macvlan device");
  if (vsystem("ip link set %s netns %d", net_vdev, child))
    critical("Failed to set namespace of network device");
  write(controlfd[1], "a", 1);

  waitpid(child, &status, 0);

  return status;
}
